# barrage

## 如果项目帮助到了您，请记得点个start哦  ^_^
[![star](https://gitee.com/MonkeyBrothers/barrage/badge/star.svg?theme=dark)](https://gitee.com/MonkeyBrothers/barrage/stargazers)
[![fork](https://gitee.com/MonkeyBrothers/barrage/badge/fork.svg?theme=dark)](https://gitee.com/MonkeyBrothers/barrage/members)
[![Fork me on Gitee](https://gitee.com/MonkeyBrothers/barrage/widgets/widget_6.svg)](https://gitee.com/MonkeyBrothers/barrage)

## go版本地址
https://gitee.com/MonkeyBrothers/barrage-go.git

### 演示地址
http://barrage.houry.top/

## v2.x版本功能

* 黑名单拦截功能
* 敏感词汇过滤
* 断网重连
* 设置弹幕颜色
* 标识自己发送的弹幕
* 弹幕位置固定功能
* 历史弹幕列表
* 历史观看总次数展示
* 标题展示
* 音量调节
* 总弹幕展示
* 实时在线观看人数展示
* 弹幕位置固定功能


### 技术架构
后端基于springboot-2.4.3、redis、netty、protocol buffer。

前端基于vue、webSocket技术实现。

### 关于项目clone之后  BarrageProto报错 缺失 的问题请参考
https://gitee.com/MonkeyBrothers/barrage/issues/I5L0XP


### 特别说明
1.此版本为前后段分离的项目，后端代码第一阶段已经完毕。目前项目就本人自己维护，有很多还不成熟的地方，欢迎大家集思广益共同维护！

2.项目会一直维护下去，不定时更新。

3.目前1.0版本和2.X版本 已经发布。 2.X版本采用前后端分离的框架进行设计。


### 前端地址
https://gitee.com/MonkeyBrothers/barrage-vue

### 安装教程
1.执行命令
```shell
mvn clean compile
```
2.启动springboot

3.启动前端服务

### 历史版本

[前后端分离版本]-[V2.0版本] https://gitee.com/MonkeyBrothers/barrage/tree/V2.0/

[前后端未分离版本]-[V1.0版本] https://gitee.com/MonkeyBrothers/barrage/tree/V1.0/

### V2.1.0版本效果截图
![avatar](/images/v2.1.0/1.png)
![avatar](/images/v2.1.0/2.png)
![avatar](/images/v2.1.0/3.png)

### V2.0版本效果截图
![avatar](/images/v2.0/1.png)
![avatar](/images/v2.0/2.png)
![avatar](/images/v2.0/3.png)


### V1.0版本效果截图
![avatar](/images/v1.0/1.png)
![avatar](/images/v1.0/2.png)
![avatar](/images/v1.0/3.png)

### 有任何问题都可以添加我的个人微信
![avatar](/images/v1.0/vx.jpg)
### 关注我
![avatar](/images/v1.0/WeChat.png)
